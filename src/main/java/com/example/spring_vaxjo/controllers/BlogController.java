package com.example.spring_vaxjo.controllers;

import com.example.spring_vaxjo.dto.BlogDTOConverter;
import com.example.spring_vaxjo.dto.BlogRequestDTO;
import com.example.spring_vaxjo.dto.BlogResponseDTO;
import com.example.spring_vaxjo.entities.BlogPost;
import com.example.spring_vaxjo.repositories.BlogPostRepository;
import com.example.spring_vaxjo.service.BlogPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/blog")
public class BlogController {

    //@Autowired
    //BlogPostService blogPostService;

    BlogPostService blogPostService;
    BlogDTOConverter blogDTOConverter;

    public BlogController(BlogPostService blogPostService, BlogDTOConverter blogDTOConverter){
        this.blogPostService = blogPostService;
        this.blogDTOConverter = blogDTOConverter;
    }

    @GetMapping
    public String getBlogPostList(Model model){
        List<BlogPost> blogPostList = blogPostService.findAll("");
        blogPostList.forEach(System.out::println);
        model.addAttribute("blogPostList", blogPostList);
        return "blog";
    }

    @GetMapping("/{id}")
    public BlogResponseDTO getBlogPostById(@PathVariable("id") int id){
        BlogPost blogPost = blogPostService.findPostById(id);
        return blogDTOConverter.entityToBlogResponseDTO(blogPost);
    }

    @DeleteMapping("/{id}")
    public void deleteBlogPostById(@PathVariable("id") int id){
        blogPostService.deleteById(id);
    }

    @PostMapping
    public BlogResponseDTO createBlogPost(@RequestBody BlogRequestDTO blogRequestDTO){
        BlogPost blogPost = blogDTOConverter.BlogRequestDTOToEntity(blogRequestDTO);
        blogPost = blogPostService.save(blogPost);
        return blogDTOConverter.entityToBlogResponseDTO(blogPost);
    }

    @PutMapping("/{id}")
    public BlogResponseDTO updateById(
            @RequestBody BlogRequestDTO changedBlogPostDTO,
            @PathVariable("id") int id){

        BlogPost changedBlogPost = blogDTOConverter
                .BlogRequestDTOToEntity(changedBlogPostDTO);

        BlogPost blogpost = blogPostService.updateById(id, changedBlogPost);
        return blogDTOConverter.entityToBlogResponseDTO(blogpost);

    }





}
