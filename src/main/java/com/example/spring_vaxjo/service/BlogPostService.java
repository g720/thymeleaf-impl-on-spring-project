package com.example.spring_vaxjo.service;

import com.example.spring_vaxjo.entities.BlogPost;
import com.example.spring_vaxjo.repositories.BlogPostRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@Service
public class BlogPostService {

    BlogPostRepository blogPostRepository;

    public BlogPostService(BlogPostRepository blogPostRepository) {
        this.blogPostRepository = blogPostRepository;
    }

    public List<BlogPost> findAll(String username) {
        /*
        Stream<BlogPost> blogPostStream = blogPostRepository.findAll().stream();
        if(username != null){
          blogPostStream = blogPostStream.filter(blogPost -> blogPost.getAppUser().getUsername().equals(username));
        }
        return blogPostStream.toList();
         */

        return blogPostRepository.findAll();

    }

    public BlogPost findPostById(int id) {
        return blogPostRepository.findById(id).orElseThrow();
    }

    public void deleteById(int id) {
        blogPostRepository.deleteById(id);
    }

    public BlogPost save(BlogPost blogPost) {
        return blogPostRepository.save(blogPost);
    }

    public BlogPost updateById(int id, BlogPost changedBlogPost) {

        BlogPost existingBlogPost = blogPostRepository.findById(id).orElseThrow();

        if(changedBlogPost.getTitle() != null)
            existingBlogPost.setTitle(changedBlogPost.getTitle());
        if(changedBlogPost.getMessage() != null)
            existingBlogPost.setMessage(changedBlogPost.getMessage());

        blogPostRepository.save(existingBlogPost);

        return existingBlogPost;
    }
}
