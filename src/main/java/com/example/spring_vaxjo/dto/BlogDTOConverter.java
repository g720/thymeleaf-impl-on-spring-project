package com.example.spring_vaxjo.dto;

import com.example.spring_vaxjo.entities.AppUser;
import com.example.spring_vaxjo.entities.BlogPost;
import com.example.spring_vaxjo.repositories.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlogDTOConverter {

    @Autowired
    AppUserRepository appUserRepository;

    public BlogPost BlogRequestDTOToEntity(BlogRequestDTO blogRequestDTO){

        AppUser appUser = appUserRepository
                .findById(blogRequestDTO.getAppuser_id())
                .orElseThrow();

        return new BlogPost(blogRequestDTO.getTitle(),
                            blogRequestDTO.getMessage(),
                            appUser
        );
    }

    public BlogResponseDTO entityToBlogResponseDTO(BlogPost blogPost){

        return new BlogResponseDTO(
                blogPost.getId(),
                blogPost.getTitle(),
                blogPost.getMessage(),
                blogPost.getAppUser().getId()
        );

    }

}
