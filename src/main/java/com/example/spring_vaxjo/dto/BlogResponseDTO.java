package com.example.spring_vaxjo.dto;

public class BlogResponseDTO {

    private int id;
    private String title;
    private String message;
    private int appUserId;

    public BlogResponseDTO(int id, String title, String message, int appUserId) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.appUserId = appUserId;
    }

    public BlogResponseDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(int appUserId) {
        this.appUserId = appUserId;
    }
}
