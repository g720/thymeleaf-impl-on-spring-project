package com.example.spring_vaxjo;

import com.example.spring_vaxjo.entities.AppUser;
import com.example.spring_vaxjo.entities.BlogPost;
import com.example.spring_vaxjo.repositories.AppUserRepository;
import com.example.spring_vaxjo.repositories.BlogPostRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringVaxjoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringVaxjoApplication.class, args);
    }

    @Bean
    CommandLineRunner init(
            BlogPostRepository blogPostRepository,
            AppUserRepository appUserRepository){
        return args -> {


            AppUser appUser = new AppUser("Gunnar");
            appUserRepository.save(appUser);

            BlogPost blogPost = new BlogPost("Post 1", "Brödtext 1", appUser);
            blogPostRepository.save(blogPost);
            BlogPost blogPost2 = new BlogPost("Post 2", "Var ute och fiskade idag", appUser);
            blogPostRepository.save(blogPost2);

        };
    }

}
